#include <iostream>
#include <vector>
#include <set>
#include <optional>


class Solution {
public:
    std::optional<std::vector<int>> Solve(int n) {
        std::vector<int> winning_positions;
        std::vector<unsigned> nim_type(n + 1);
        nim_type[0] = 0;
        nim_type[1] = 0;
        for (int k = 2; k <= n; ++k) {
            std::vector<unsigned> types;
            for (int i = 0; i < k; ++i) {
                int before = i;
                int after = k - i - 1;
                types.push_back(nim_type[after] ^ nim_type[before]);
                if ((nim_type[after] ^ nim_type[before]) == 0 && k == n) {
                    winning_positions.push_back(i + 1);
                }
            }
            nim_type[k] = Mex(types);
        }
        if (nim_type[n] == 0) {
            return std::nullopt;
        }
        return winning_positions;
    }

private:
    //minimum excluded value
    int Mex(const std::vector<unsigned>& numbers) {
        std::set<int> unique_numbers(numbers.begin(), numbers.end());
        int answer = 0;
        for (auto number: unique_numbers) {
            if (answer < number) {
                return answer;
            }
            ++answer;
        }
        return answer;
    }
};



int main() {
    int n;
    std::cin >> n;
    Solution solution;
    auto result = solution.Solve(n);
    if (result.has_value()) {
        std::cout << "Schtirlitz" << std::endl;
        for (int i: result.value()) {
            std::cout << i << std::endl;
        }
    } else {
        std::cout << "Mueller" << std::endl;
    }
}
