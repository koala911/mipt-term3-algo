#include <iostream>

template <typename T>
class Vector {
public:
    explicit Vector(T x = 0, T y = 0): x_(x), y_(y) {}

    Vector& operator+=(const Vector& other) {
        x_ += other.x_;
        y_ += other.y_;
        return *this;
    }

    Vector& operator-=(const Vector& other) {
        x_ -= other.x_;
        y_ -= other.y_;
        return *this;
    }

    Vector& operator*=(T coefficient) {
        x_ *= coefficient;
        y_ *= coefficient;
        return *this;
    }

    Vector& operator/=(T coefficient) {
        x_ /= coefficient;
        y_ /= coefficient;
        return *this;
    }

    T operator*(const Vector& other) const {
        return (x_ * other.x_ + y_ * other.y_);
    }

    T operator%(const Vector& other) const {
        return (x_ * other.y_ - y_ * other.x_);
    }

    Vector operator+(const Vector& other) const {
        auto result = *this;
        result += other;
        return result;
    }

    Vector operator-(const Vector& other) const {
        auto result = *this;
        result -= other;
        return result;
    }

    Vector operator*(T coefficient) {
        auto result = *this;
        result *= coefficient;
        return result;
    }

    Vector operator/(T coefficient) {
        auto result = *this;
        result /= coefficient;
        return result;
    }

    bool operator==(const Vector& other) const {
        return (x_ == other.x_ && y_ == other.y_);
    }

    bool IsCollinearTo(const Vector& other) const {
        return (*this % other == 0);
    }

    T GetX() const {
        return x_;
    }

    T GetY() const {
        return y_;
    }
private:
    T x_;
    T y_;
};


template <typename T>
Vector<T> operator*(T coefficient, const Vector<T>& vector) {
    return vector * coefficient;
}


template <typename T>
bool IsCollinear(const Vector<T>& left, const Vector<T>& right) {
    return left.IsCollinearTo(right);
}


template <typename T>
using Point = Vector<T>;


template <typename T>
std::istream& operator>>(std::istream& in, Point<T>& point) {
    T x;
    T y;
    in >> x >> y;
    point = Point<T>(x, y);
    return in;
}


template <typename T>
class Line {
public:
    Line(const Point<T>& point, const Vector<T>& vector): point_(point), vector_(vector) {}


    bool IsParallelTo(const Line& other) const {
        return vector_.IsCollinearTo(other.vector_);
    }

    bool operator==(const Line& other) const {
        return (IsParallelTo(other) && vector_.IsCollinearTo(point_ - other.point_));
    }

    bool operator!=(const Line& other) const {
        return !(*this == other);
    }

    bool IsIntersectingWith(const Line& other) const {
        return (*this == other || !IsParallelTo(other));
    }

    // numerator and denominator of t: x = point_ + t * vector_, where x - point of intersection
    std::pair<T, T> Intersection(const Line& other) const {
        T numerator = other.vector_ % (point_ - other.point_);
        T denominator = vector_ % other.vector_;
        if (denominator < 0) {
            denominator *= -1;
            numerator *= -1;
        }
        return std::make_pair(numerator, denominator);
    }

    // numerator and denominator of coordinate t: point = point_ + t * vector_
    std::pair<T, T> CoordinateOfPoint(const Point<T>& point) const {
        T numerator = point.GetX() - point_.GetX();
        T denominator = vector_.GetX();
        if (denominator < 0) {
            denominator *= -1;
            numerator *= -1;
        }
        return std::make_pair(numerator, denominator);
    }
private:
    // L = {a + t*v: t \in R}, a - point, v - vector, v != 0
    Point<T> point_;
    Vector<T> vector_;
};


template <typename T>
class Segment {
public:
    Segment() = default;
    Segment(const Point<T>& point, const Vector<T>& vector): point_(point), vector_(vector) {}

    bool IsIntersectingWith(const Line<T>& other_line) const {
        Line<T> line(point_, vector_);
        if (line.IsParallelTo(other_line)) {
            return (line == other_line);
        }
        auto pair = line.Intersection(other_line);
        T numerator = pair.first;
        T denominator = pair.second;
        return (numerator >= 0 && numerator <= denominator);
    }

    bool IsIntersectingWith(const Segment& other) const {
        Line<T> line(point_, vector_);
        Line<T> other_line(other.point_, other.vector_);
        if (line.IsParallelTo(other_line)) {
            if (line != other_line) {
                return false;
            }
            auto pair = line.CoordinateOfPoint(other.point_);
            T numerator_start = pair.first;
            T denominator_start = pair.second;
            pair = line.CoordinateOfPoint(other.point_ + other.vector_);
            T numerator_end = pair.first;
            T denominator_end = pair.second;
            // we want t_start <= t_end
            if (numerator_start * denominator_end > numerator_end * denominator_start) {
                std::swap(numerator_start, numerator_end);
                std::swap(denominator_start, denominator_end);
            }
            // [0, 1] \cap [t_start, t_end] != \varnothing <=> (t_start <= 1 and t_end >= 0)
            return (numerator_start <= denominator_start && numerator_end >= 0);
        }
        return (IsIntersectingWith(other_line) && other.IsIntersectingWith(line));
    }

private:
    // S = {a + t*v: t \in [0, 1]}, a - point, v - vector
    Point<T> point_;
    Vector<T> vector_;
};


template <typename T>
std::istream& operator>>(std::istream& in, Segment<T>& segment) {
    Point<T> start_point;
    Point<T> end_point;
    in >> start_point >> end_point;
    segment = Segment<T>(start_point, end_point - start_point);
    return in;
}

int main() {
    Segment<int> road;
    int n;
    std::cin >> road >> n;
    Segment<int> current_road;
    int result = 0;
    for (int i = 0; i < n; ++i) {
        std::cin >> current_road;
        if (road.IsIntersectingWith(current_road)) {
            ++result;
        }
    }
    std::cout << result;
}
