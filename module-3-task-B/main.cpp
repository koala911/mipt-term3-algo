#include <iostream>
#include <vector>
#include <algorithm>
#include <cmath>
#include <iomanip>
#include <cstdint>


template <typename T>
class Vector {
public:
    explicit Vector(T x = 0, T y = 0): x_(x), y_(y) {}

    Vector& operator+=(const Vector& other) {
        x_ += other.x_;
        y_ += other.y_;
        return *this;
    }

    Vector& operator-=(const Vector& other) {
        x_ -= other.x_;
        y_ -= other.y_;
        return *this;
    }

    Vector& operator*=(T coefficient) {
        x_ *= coefficient;
        y_ *= coefficient;
        return *this;
    }

    Vector& operator/=(T coefficient) {
        x_ /= coefficient;
        y_ /= coefficient;
        return *this;
    }

    T operator*(const Vector& other) const {
        return (x_ * other.x_ + y_ * other.y_);
    }

    T operator%(const Vector& other) const {
        return (x_ * other.y_ - y_ * other.x_);
    }

    Vector operator+(const Vector& other) const {
        auto result = *this;
        result += other;
        return result;
    }

    Vector operator-(const Vector& other) const {
        auto result = *this;
        result -= other;
        return result;
    }

    Vector operator*(T coefficient) {
        auto result = *this;
        result *= coefficient;
        return result;
    }

    Vector operator/(T coefficient) {
        auto result = *this;
        result /= coefficient;
        return result;
    }

    bool operator==(const Vector& other) const {
        return (x_ == other.x_ && y_ == other.y_);
    }

    bool IsCollinearTo(const Vector& other) const {
        return (*this % other == 0);
    }

    T GetX() const {
        return x_;
    }

    T GetY() const {
        return y_;
    }

    T SquareOfLength() const {
        return x_ * x_ + y_ * y_;
    }
private:
    T x_;
    T y_;
};


template <typename T>
Vector<T> operator*(T coefficient, const Vector<T>& vector) {
    return vector * coefficient;
}


template <typename T>
bool IsCollinear(const Vector<T>& left, const Vector<T>& right) {
    return left.IsCollinearTo(right);
}


template <typename T>
using Point = Vector<T>;


template <typename T>
std::istream& operator>>(std::istream& in, Point<T>& point) {
    T x;
    T y;
    in >> x >> y;
    point = Point<T>(x, y);
    return in;
}


template <typename T>
class Solution {
public:
    explicit Solution(std::vector<Point<T>>&& points): points_(points) {}

    double CalculateLength() {
        FindConvexHull();
        double length = 0;
        for (int i = 0; i < convex_hull_.size(); ++i) {
            auto current_vector = convex_hull_[(i + 1) % convex_hull_.size()] - convex_hull_[i];
            length += std::sqrt(current_vector.SquareOfLength());
        }
        return length;
    }

private:
    std::vector<Point<T>> points_;
    std::vector<Point<T>> convex_hull_;

    void FindConvexHull() {
        auto first_point = *std::min_element(points_.begin(), points_.end(), [](const Point<T>& left, const Point<T>& right){
            return (left.GetY() < right.GetY() || (left.GetY() == right.GetY() && left.GetX() < right.GetX()));
        });
        std::sort(points_.begin(), points_.end(), Comparator(first_point));
        convex_hull_.push_back(first_point);
        int i = 1;
        while (i < points_.size() && points_[i] == first_point) {
            ++i;
        }
        if (i == points_.size()) {
            return;
        }
        convex_hull_.push_back(points_[i]);
        for(++i; i < points_.size(); ++i) {
            auto current_point = points_[i];
            auto last_point = convex_hull_.back();
            auto penult_point = convex_hull_[convex_hull_.size() - 2];
            while (convex_hull_.size() >= 3 && (current_point - last_point) % (last_point - penult_point) >= 0) {
                convex_hull_.pop_back();
                last_point = convex_hull_.back();
                penult_point = convex_hull_[convex_hull_.size() - 2];
            }
            convex_hull_.push_back(current_point);
        }
    }

    struct Comparator {
        Comparator(const Point<T>& point): first_point(point) {}

        Point<T> first_point;

        bool operator()(const Point<T>& left, const Point<T>& right) const {
            Vector<T> vector_left = left - first_point;
            Vector<T> vector_right = right - first_point;
            if (vector_left % vector_right != 0) {
                return (vector_left % vector_right > 0);
            }
            return vector_left.SquareOfLength() < vector_right.SquareOfLength();
        }
    };
};


int main() {
    int n;
    std::cin >> n;
    std::vector<Point<int64_t>> points(n);
    for (auto& point: points) {
        std::cin >> point;
    }
    Solution<int64_t> solution(std::move(points));
    std::cout << std::setprecision(std::numeric_limits<double>::digits10 + 1) << solution.CalculateLength() << std::endl;
}
