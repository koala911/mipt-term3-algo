#include <iostream>
#include <vector>
#include <queue>
#include <memory>
#include <unordered_map>
#include <cstdint>


class Trie {
public:
    explicit Trie(const std::vector<std::string>& patterns): root_(std::make_shared<Vertex>()), current_vertex_(root_) {
        ConstructEdges(patterns);
        ConstructSuffixReferences();
        ConstructCompressedSuffixReferences();
    }

    void Go(char c) {
        while (current_vertex_ != nullptr) {
            if (current_vertex_->edges.find(c) != current_vertex_->edges.end()) {
                current_vertex_ = current_vertex_->edges[c];
                return;
            } else {
                current_vertex_ = current_vertex_->suffix_reference.lock();
            }
        }
        current_vertex_ = root_;
    }

    std::vector<int> FindPatterns() {
        std::vector<int> found;
        auto current = current_vertex_;
        while (current != nullptr) {
            if (current->accept) {
                found.push_back(current->word_index);
            }
            current = current->compressed_suffix_reference.lock();
        }
        return found;
    }

private:
    struct Vertex {
        Vertex(): suffix_reference(), parent(), compressed_suffix_reference(), accept(false), previous_symbol('$'), word_index(-1) {}

        std::unordered_map<char, std::shared_ptr<Vertex>> edges;
        std::weak_ptr<Vertex> suffix_reference;
        std::weak_ptr<Vertex> compressed_suffix_reference;
        std::weak_ptr<Vertex> parent;
        int word_index;
        char previous_symbol;
        bool accept;
    };

    std::shared_ptr<Vertex> root_;
    std::shared_ptr<Vertex> current_vertex_;

    void ConstructEdges(const std::vector<std::string>& patterns) {
        for (int i = 0; i < patterns.size(); ++i) {
            const auto& s = patterns[i];
            auto current = root_;
            std::shared_ptr<Vertex> previous = nullptr;
            for (char c: s) {
                previous = current;
                current = current->edges[c];
                if (current == nullptr) {
                    current = std::make_shared<Vertex>();
                    previous->edges[c] = current;
                    current->previous_symbol = c;
                    current->parent = previous;
                }
            }
            current->accept = true;
            current->word_index = i;
        }
    }

    void ConstructSuffixReferences() {
        std::queue<std::shared_ptr<Vertex>> vertices;
        if (root_ != nullptr) {
            for (const auto& child: root_->edges) {
                if (child.second != nullptr) {
                    vertices.push(child.second);
                }
            }
        }
        while (!vertices.empty()) {
            auto current = vertices.front();
            vertices.pop();
            auto result = current->parent.lock()->suffix_reference;
            char symbol = current->previous_symbol;
            while (result.lock() != nullptr && result.lock()->edges.find(symbol) == result.lock()->edges.end()) {
                result = result.lock()->suffix_reference;
            }
            result = (result.lock() == nullptr) ? root_ : result.lock()->edges[symbol];
            current->suffix_reference = result;
            for (const auto& child: current->edges) {
                if (child.second != nullptr) {
                    vertices.push(child.second);
                }
            }
        }
    }

    void ConstructCompressedSuffixReferences() {
        std::queue<std::shared_ptr<Vertex>> vertices;
        if (root_ != nullptr) {
            for (const auto& child: root_->edges) {
                if (child.second != nullptr) {
                    vertices.push(child.second);
                }
            }
        }
        while (!vertices.empty()) {
            auto current = vertices.front();
            vertices.pop();
            if (current->suffix_reference.lock()->accept) {
                current->compressed_suffix_reference = current->suffix_reference;
            } else {
                current->compressed_suffix_reference = current->suffix_reference.lock()->compressed_suffix_reference;
            }
            for (const auto& child: current->edges) {
                if (child.second != nullptr) {
                    vertices.push(child.second);
                }
            }
        }
    }
};


template <char unknown_symbol>
class Solution {
public:
    Solution(const std::string& s, const std::string& p): text_(s) {
        std::string current;
        int32_t position = 0;
        for (char c: p) {
            if (c == unknown_symbol) {
                if (!current.empty()) {
                    AddPattern(current, position);
                    current.clear();
                }
            } else {
                current += c;
            }
            ++position;
        }
        if (!current.empty()) {
            AddPattern(current, position);
        }
        end_ = text_.size();
        int32_t i = (int32_t)(p.size()) - 1;
        while (i > 0 && p[i] == unknown_symbol) {
            --i;
            --end_;
        }
    }

    std::vector<int32_t> FindOccurrences(){
        Trie trie(patterns_);
        count_.resize(text_.size());
        std::vector<int> found_patterns;
        for (int32_t i = 0; i < end_; ++i) {
            trie.Go(text_[i]);
            found_patterns = trie.FindPatterns();
            for (int index: found_patterns) {
                const auto& pattern = patterns_[index];
                for (int32_t start_position: patterns_positions_[pattern]) {
                    if (i - start_position - (int32_t)(pattern.size()) + 1 >= 0) {
                        ++count_[i - start_position - (int32_t)(pattern.size()) + 1];
                    }
                }
            }
        }
        std::vector<int32_t> answer;
        for (int32_t i = 0; i < end_; ++i) {
            if (count_[i] == patterns_.size()) {
                answer.push_back(i);
            }
        }
        return answer;
    }


private:
    const std::string& text_;
    std::vector<std::string> patterns_;
    std::vector<int32_t> count_;
    std::unordered_map<std::string, std::vector<int32_t>> patterns_positions_;
    int32_t end_;

    void AddPattern(const std::string& current, int32_t position) {
        if (patterns_positions_.find(current) == patterns_positions_.end()) {
            patterns_positions_.insert(std::make_pair(current, std::vector<int32_t>({position - (int32_t)(current.size())})));
        } else {
            patterns_positions_[current].push_back(position - (int32_t)(current.size()));
        }
        patterns_.push_back(current);
    }
};

int main() {
    std::string text;
    std::string pattern;
    std::cin >> pattern >> text;
    Solution<'?'> solution(text, pattern);
    auto answer = solution.FindOccurrences();
    for (auto i: answer) {
        std::cout << i << " ";
    }
}
