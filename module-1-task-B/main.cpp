#include <iostream>
#include <string>
#include <vector>
#include <cstdint>


class Solution {
public:
    explicit Solution(std::string&& s): text_(s) {}

    int64_t FindPalindromes() {
        palindromes_.resize(2);
        Calculate(0);
        Calculate(1);
        int64_t answer = 0;
        for (int i = 0; i < 2; ++i) {
            for (int d: palindromes_[i]) {
                answer += d;
            }
        }
        return answer - text_.size();
    }

private:
    std::string text_;
    std::vector<std::vector<int>> palindromes_; // palindromes_[0][i] - number of palindromes of odd length with center in text[i]
                                                // palindromes_[1][i] - number of palindromes of even length with center in text[i]

    void Calculate(int is_even) {
        // is_even == 0 if calculate odd palindromes
        // is_even == 1 if calculate even palindromes
        palindromes_[is_even].resize(text_.size());
        int left = 0;
        int right = -1;
        for (int i = 0; i < text_.size(); i++) {
            int current = 0;
            if (i > right) {
                current = 1 - is_even;
            }
            else {
                current = std::min(palindromes_[is_even][left + right - i + is_even], right - i + is_even);
            }
            while (0 <= i - current - is_even && i + current < text_.size() && text_[i - current - is_even] == text_[i + current]) {
                current++;
            }
            palindromes_[is_even][i] = current;
            if (i + current - 1 > right) {
                right = i + current - 1;
                left = i - current + 1 - is_even;
            }
        }
    }
};

int main() {
    std::string s;
    std::cin >> s;
    Solution solution(std::move(s));
    std::cout << solution.FindPalindromes();
}
