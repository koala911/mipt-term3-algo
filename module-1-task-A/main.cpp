#include <iostream>
#include <string>
#include <vector>
#include <deque>
#include <iterator>
#include <sstream>


template <class InputIterator, class OutputIterator>
class Solution {
public:
    explicit Solution(std::string&& p, InputIterator&& input_begin, InputIterator&& input_end, OutputIterator&& output):
            pattern_(std::move(p)), input_begin_(input_begin), input_end_(input_end), output_(output), last_symbol_(pattern_[0]) {}

    void FindOccurrences() {
        CalculateZFunctionPattern();
        CalculateZFunctionText();
    }


private:
    std::string pattern_;
    std::vector<int> z_function_pattern_;
    int total_read_size_ = 0;
    bool text_read_ = false;
    InputIterator input_begin_;
    InputIterator input_end_;
    OutputIterator output_;


    struct Symbol {
        explicit Symbol(char c): symbol(c), is_blank(false) {}
        explicit Symbol(bool blank): is_blank(blank), symbol(0) {}

        bool operator==(const Symbol& other) {
            return (is_blank == other.is_blank) && (symbol == other.symbol);
        }

        char symbol;
        bool is_blank;
    };

    Symbol last_symbol_;
    int last_symbol_index_ = 1;
    std::deque<Symbol> current_text_;


    void CalculateZFunctionPattern() {
        z_function_pattern_.resize(pattern_.size() + 1);
        z_function_pattern_[0] = 0;
        int l = 0;
        int r = 0;
        for (int i = 1; i < pattern_.size(); ++i) {
            if (i <= r) {
                z_function_pattern_[i] = std::min(r - i + 1, z_function_pattern_[i - l]);
            }
            while (i + z_function_pattern_[i] < pattern_.size() &&
                    pattern_[z_function_pattern_[i]] == pattern_[i + z_function_pattern_[i]]) {
                ++z_function_pattern_[i];
            }
            if (i + z_function_pattern_[i] - 1 > r) {
                l = i;
                r = i + z_function_pattern_[i] - 1;
            }
        }
        z_function_pattern_[pattern_.size()] = 0;
    }

    void CalculateZFunctionText() {
        int l = 0;
        int r = 0;
        last_symbol_index_ = pattern_.size() + 1;
        last_symbol_ = Symbol(ReadNextSymbol());
        current_text_.push_back(last_symbol_);
        for (int i = pattern_.size() + 1; NotTooBig(i); ++i) {
            int current_value = 0;
            if (i <= r) {
                current_value = std::min(r - i + 1, z_function_pattern_[i - l]);
            }
            while (last_symbol_index_ < i) {
                last_symbol_ = Symbol(ReadNextSymbol());
                ++last_symbol_index_;
                if (last_symbol_index_ == i) {
                    current_text_.push_back(last_symbol_);
                }
            }
            while (NotTooBig(i + current_value) && current_value < pattern_.size() &&
                    GetSymbol(current_value) == current_text_[current_value]) {
                ++current_value;
                last_symbol_ = Symbol(ReadNextSymbol());
                ++last_symbol_index_;
                current_text_.push_back(last_symbol_);
            }
            if (i + current_value - 1 > r) {
                l = i;
                r = i + current_value - 1;
            }
            current_text_.pop_front();
            if (current_value == pattern_.size()) {
                *output_ = i - int(pattern_.size()) - 1;
            }
        }
    }

    char ReadNextSymbol() {
        if (input_begin_ != input_end_) {
            char next_symbol = *input_begin_;
            ++input_begin_;
            ++total_read_size_;
            if (input_begin_ == input_end_) {
                text_read_ = true;
            }
            return next_symbol;
        } else {
            text_read_ = true;
            return 0;
        }
    }

    Symbol GetSymbol(int i) {
        if (i < pattern_.size()) {
            return Symbol(pattern_[i]);
        } else if (i == pattern_.size()) {
            return Symbol(true);
        }
    }

    // i < |pattern| + 1 + |text|
    bool NotTooBig(int i) {
        return (!text_read_ || (i < total_read_size_ + 1 + pattern_.size()));
    }
};

int main() {
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(NULL);
    std::string pattern;
    std::cin >> pattern;
    Solution<std::istream_iterator<char>, std::ostream_iterator<int>> solution(std::move(pattern), std::istream_iterator<char>(std::cin), std::istream_iterator<char>(), std::ostream_iterator<int>(std::cout, " "));
    solution.FindOccurrences();
}






