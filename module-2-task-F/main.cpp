#include <iostream>
#include <memory>
#include <map>
#include <stack>
#include <cstdint>
#include <vector>
#include <queue>


class Solution {
public:
    Solution(std::string&& s, std::string&& t): s_(std::move(s)), t_(std::move(t)) {}

    std::string Start(int64_t k) {
        s_ += '$';
        t_ += '#';
        SuffixTree tree(s_ + t_);
        tree.DeleteSecondString(s_.size());
        tree.DeleteUniqueSuffixes(s_, t_);
        tree.CalculateSizes();
        return tree.FindKSubstring(k, s_, t_);
    }

private:
    std::string s_;
    std::string t_;
    class SuffixTree {
    public:
        explicit SuffixTree(std::string&& s): blank_(std::make_shared<Vertex>()), root_(std::make_shared<Vertex>()), text_(std::move(s)) {
            current_vertex_ = root_;
            for (i = 0; i < text_.size(); ++i) {
                blank_->edges[text_[i]] = std::make_shared<Edge>(i, i + 1, blank_, root_);
            }
            root_->previous_vertex = blank_;
            root_->suffix_reference = blank_;
            root_->previous_symbol = text_[0];
            for (i = 0; i < text_.size(); ++i) {
                char c = text_[i];
                while (true) {
                    if (CanGo(c)) {
                        Go(c);
                        break;
                    } else {
                        auto new_edge = CreateEdge(c);
                        auto previous_vertex = new_edge->start_vertex;
                        GoBySuffixReference();
                        previous_vertex->suffix_reference = current_vertex_;
                    }
                }
            }
        }

        void DeleteSecondString(int first_string_size) {
            std::stack<std::shared_ptr<Vertex>> vertices;
            vertices.push(root_);
            while (!vertices.empty()) {
                auto current = vertices.top();
                vertices.pop();
                for (const auto& p: current->edges) {
                    if (p.second->start_index >= first_string_size || p.second->end_index <= first_string_size) {
                        if (p.second->start_index >= first_string_size) {
                            p.second->start_index -= first_string_size;
                            p.second->end_index -= first_string_size;
                            p.second->in_t = true;
                        }
                        vertices.push(p.second->end_vertex);
                    } else {
                        p.second->end_index = first_string_size;
                        p.second->end_vertex->edges.clear();
                    }
                }
            }
        }

        void DeleteUniqueSuffixes(const std::string& s, const std::string& t) {
            CalculateReachable(root_, 0, s, t);
            std::queue<std::shared_ptr<Vertex>> vertices;
            vertices.push(root_);
            while (!vertices.empty()) {
                auto current = vertices.front();
                vertices.pop();
                std::map<char, std::shared_ptr<Edge>, std::less<>> new_edges;
                for (const auto& p: current->edges) {
                    auto next_vertex = p.second->end_vertex;
                    if (next_vertex->reachable_dollar && next_vertex->reachable_sharp) {
                        vertices.push(next_vertex);
                        new_edges.insert(p);
                    }
                }
                current->edges = std::move(new_edges);
            }
        }

        void CalculateSizes() {
            CalculateSizesRecursive(root_);
        }

        std::string FindKSubstring(int64_t k, const std::string& s, const std::string& t) {
            std::string answer;
            auto current = root_;
            while (true) {
                bool flag = false;
                for (const auto& p: current->edges) {
                    auto edge = p.second;
                    auto next_vertex = edge->end_vertex;
                    if (k == 0) {
                        return answer;
                    }
                    if (k <= edge->Length()) {
                        for (int j = 0; j < k; ++j) {
                            if (edge->in_t) {
                                answer += t[edge->start_index + j];
                            } else {
                                answer += s[edge->start_index + j];
                            }
                        }
                        flag = true;
                        return answer;
                    }
                    if (k <= edge->Length() + next_vertex->size) {
                        for (int j = 0; j < edge->Length(); ++j) {
                            if (edge->in_t) {
                                answer += t[edge->start_index + j];
                            } else {
                                answer += s[edge->start_index + j];
                            }
                        }
                        k -= edge->Length();
                        current = next_vertex;
                        flag = true;
                        break;
                    } else {
                        k -= edge->Length() + next_vertex->size;
                    }
                }
                if (!flag) {
                    return "-1";
                }
            }
        }

    private:
        struct Edge;
        struct Vertex;

        struct Vertex {
            Vertex(): number(counter++) {}

            std::map<char, std::shared_ptr<Edge>, std::less<>> edges;

            std::weak_ptr<Vertex> suffix_reference;
            char previous_symbol;
            std::weak_ptr<Vertex> previous_vertex;
            static int counter;
            int number;
            int64_t size = 0;
            bool reachable_dollar = false;
            bool reachable_sharp = false;
        };

        // text_[start_index ... end_index)
        struct Edge {
            Edge() = default;
            Edge(int s_i, int e_i, std::shared_ptr<Vertex> s_v, std::shared_ptr<Vertex> e_v):
                    start_index(s_i), end_index(e_i), start_vertex(std::move(s_v)), end_vertex(std::move(e_v)) {}


            int start_index;
            int end_index;
            std::shared_ptr<Vertex> start_vertex;
            std::shared_ptr<Vertex> end_vertex;
            bool in_t = false;

            Edge& operator=(const Edge& other) {
                start_index = other.start_index;
                end_index = other.end_index;
                start_vertex = other.start_vertex;
                end_vertex = other.end_vertex;
                return *this;
            }

            int Length() const {
                return end_index - start_index;
            }

            std::shared_ptr<Edge> Split(int position, const std::string& text) {
                std::shared_ptr<Vertex> new_vertex = std::make_shared<Vertex>();
                std::shared_ptr<Edge> new_edge = std::make_shared<Edge>(start_index + position + 1, end_index, new_vertex, end_vertex);
                new_vertex->edges[text[start_index + position + 1]] = new_edge;
                new_vertex->previous_vertex = start_vertex;
                new_vertex->previous_symbol = text[start_index];
                end_index = start_index + position + 1;
                end_vertex->previous_vertex = new_vertex;
                end_vertex->previous_symbol = text[start_index + position + 1];
                end_vertex = new_vertex;
                return new_edge;
            }
        };

        std::shared_ptr<Vertex> blank_; // fictitious vertex over the root
        std::shared_ptr<Vertex> root_;
        std::string text_;
        std::shared_ptr<Edge> current_edge_;
        std::shared_ptr<Vertex> current_vertex_;
        int current_position_ = -1;
        int i = 0;

        void SetCurrentEdge(const std::shared_ptr<Edge>& new_current_edge) {
            current_edge_ = new_current_edge;
            current_vertex_.reset();
            current_position_ = 0;
            if (current_edge_->Length() == 1) {
                current_position_ = -1;
                current_vertex_ = current_edge_->end_vertex;
                current_edge_.reset();
            }
        }

        void SetCurrentVertex(const std::shared_ptr<Vertex>& new_current_vertex) {
            current_vertex_ = new_current_vertex;
            current_edge_.reset();
            current_position_ = -1;
        }

        bool CanGo(char c) {
            if (current_edge_) {
                return (text_[current_edge_->start_index + current_position_ + 1] == c);
            } else if (current_vertex_) {
                return (current_vertex_->edges.find(c) != current_vertex_->edges.end());
            } else {
                return false;
            }
        }

        void Go(char c) {
            if (current_edge_) {
                if (current_edge_->start_index + current_position_ < current_edge_->end_index - 2) {
                    ++current_position_;
                } else if (current_edge_->start_index + current_position_ == current_edge_->end_index - 2) {
                    SetCurrentVertex(current_edge_->end_vertex);
                }
            } else if (current_vertex_) {
                SetCurrentEdge(current_vertex_->edges[c]);
            }
        }

        std::shared_ptr<Edge> CreateEdge(char c) {
            if (current_vertex_) {
                std::shared_ptr<Vertex> new_vertex = std::make_shared<Vertex>();
                std::shared_ptr<Edge> new_edge = std::make_shared<Edge>(i, text_.size(), current_vertex_, new_vertex);
                current_vertex_->edges[c] = new_edge;
                new_vertex->previous_symbol = c;
                new_vertex->previous_vertex = current_vertex_;
                return new_edge;
            } else if (current_edge_) {
                std::shared_ptr<Vertex> new_start_vertex = std::make_shared<Vertex>();
                new_start_vertex->previous_vertex = current_edge_->start_vertex;
                new_start_vertex->previous_symbol = text_[current_edge_->start_index];
                std::shared_ptr<Vertex> new_end_vertex = std::make_shared<Vertex>();
                new_end_vertex->previous_vertex = new_start_vertex;
                new_end_vertex->previous_symbol = c;
                std::shared_ptr<Edge> new_edge_1 = std::make_shared<Edge>(i, text_.size(), new_start_vertex, new_end_vertex);
                new_start_vertex->edges[c] = new_edge_1;
                current_vertex_ = new_start_vertex;
                current_edge_->end_vertex->previous_vertex = new_start_vertex;
                current_edge_->end_vertex->previous_symbol = text_[current_edge_->start_index + current_position_ + 1];
                std::shared_ptr<Edge> new_edge_2 = std::make_shared<Edge>(current_edge_->start_index + current_position_ + 1, current_edge_->end_index, new_start_vertex, current_edge_->end_vertex);
                current_edge_->end_index = current_edge_->start_index + current_position_ + 1;
                current_edge_->end_vertex = new_start_vertex;
                new_start_vertex->edges[text_[current_edge_->start_index + current_position_ + 1]] = new_edge_2;
                current_edge_.reset();
                current_position_ = -1;
                return new_edge_1;
            }
        }

        std::shared_ptr<Vertex> ConstructVertex() {
            if (current_vertex_) {
                return current_vertex_;
            } else if (current_edge_) {
                if (current_position_ == current_edge_->Length() - 1) {
                    return current_edge_->end_vertex;
                }
                return current_edge_->Split(current_position_, text_)->start_vertex;
            }
        }

        void GoBySuffixReference() {
            if (current_vertex_ == root_) {
                current_vertex_ = blank_;
                return;
            }
            auto result_vertex = current_vertex_->previous_vertex.lock();
            auto result_edge = result_vertex->edges[current_vertex_->previous_symbol];
            int current_shift = result_edge->Length();
            int current_index = result_edge->start_index;
            result_vertex = result_vertex->suffix_reference.lock();
            result_edge = result_vertex->edges[text_[current_index]];
            while (current_shift >= result_edge->Length()) {
                current_shift -= result_edge->Length();
                current_index += result_edge->Length();
                result_vertex = result_edge->end_vertex;
                result_edge = result_vertex->edges[text_[current_index]];
            }
            if (current_shift > 0) {
                current_edge_ = result_edge;
                current_position_ = current_shift - 1;
                current_vertex_.reset();
            } else {
                current_vertex_ = result_vertex;
                current_edge_.reset();
                current_position_ = -1;
            }
            current_vertex_ = ConstructVertex();
        }

        void CalculateReachable(std::shared_ptr<Vertex> current, char previous_symbol, const std::string& s, const std::string& t) {
            if (previous_symbol == '$') {
                current->reachable_dollar = true;
                return;
            }
            if (previous_symbol == '#') {
                current->reachable_sharp = true;
                return;
            }
            for (const auto& p: current->edges) {
                char symbol;
                auto edge = p.second;
                auto next_vertex = edge->end_vertex;
                if (edge->in_t) {
                    symbol = t[edge->end_index - 1];
                } else {
                    symbol = s[edge->end_index - 1];
                }
                CalculateReachable(next_vertex, symbol, s, t);
                current->reachable_dollar = next_vertex->reachable_dollar || current->reachable_dollar;
                current->reachable_sharp = next_vertex->reachable_sharp || current->reachable_sharp;
            }
        }


        void CalculateSizesRecursive(std::shared_ptr<Vertex> current) {
            for (const auto& p: current->edges) {
                auto next_vertex = p.second->end_vertex;
                CalculateSizesRecursive(next_vertex);
                current->size += next_vertex->size + p.second->Length();
            }
        }
    };
};


int Solution::SuffixTree::Vertex::counter = -1;


int main() {
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(nullptr);
    std::cout.tie(nullptr);
    std::string s;
    std::string t;
    int64_t k;
    std::cin >> s >> t >> k;
    Solution solution(std::move(s), std::move(t));
    std::cout << solution.Start(k);
}
