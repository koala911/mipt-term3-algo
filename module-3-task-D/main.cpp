#include <iostream>
#include <vector>
#include <algorithm>
#include <cmath>


template <typename T>
class Vector {
public:
    explicit Vector(T x = 0, T y = 0): x_(x), y_(y) {}
    Vector(const Vector&) = default;
    Vector(Vector&&) = default;

    Vector& operator=(const Vector& other) = default;
    Vector& operator=(Vector&& other) = default;

    Vector& operator+=(const Vector& other) {
        x_ += other.x_;
        y_ += other.y_;
        return *this;
    }

    Vector& operator-=(const Vector& other) {
        x_ -= other.x_;
        y_ -= other.y_;
        return *this;
    }

    Vector& operator*=(T coefficient) {
        x_ *= coefficient;
        y_ *= coefficient;
        return *this;
    }

    Vector& operator/=(T coefficient) {
        x_ /= coefficient;
        y_ /= coefficient;
        return *this;
    }

    T operator*(const Vector& other) const {
        return (x_ * other.x_ + y_ * other.y_);
    }

    T operator%(const Vector& other) const {
        return (x_ * other.y_ - y_ * other.x_);
    }

    Vector operator+(const Vector& other) const {
        auto result = *this;
        result += other;
        return result;
    }

    Vector operator-(const Vector& other) const {
        auto result = *this;
        result -= other;
        return result;
    }

    Vector operator*(T coefficient) {
        auto result = *this;
        result *= coefficient;
        return result;
    }

    Vector operator/(T coefficient) {
        auto result = *this;
        result /= coefficient;
        return result;
    }

    bool operator==(const Vector& other) const {
        return (x_ == other.x_ && y_ == other.y_);
    }

    bool IsCollinearTo(const Vector& other) const {
        return (*this % other == 0);
    }

    T GetX() const {
        return x_;
    }

    T GetY() const {
        return y_;
    }

    T SquareOfLength() const {
        return x_ * x_ + y_ * y_;
    }
private:
    T x_;
    T y_;
};


template <typename T>
T Dot(const Vector<T>& left, const Vector<T>& right) {
    return left.Dot(right);
}


template <typename T>
Vector<T> operator*(T coefficient, const Vector<T>& vector) {
    return vector * coefficient;
}


template <typename T>
bool IsCollinear(const Vector<T>& left, const Vector<T>& right) {
    return left.IsCollinearTo(right);
}


template <typename T>
using Point = Vector<T>;


template <typename T>
std::istream& operator>>(std::istream& in, Point<T>& point) {
    T x;
    T y;
    in >> x >> y;
    point = Point<T>(x, y);
    return in;
}


template <typename T>
class Line {
public:
    Line(const Point<T>& point, const Vector<T>& vector): point_(point), vector_(vector) {}
    Line(const Line& other) = default;
    Line(Line&& other) = default;

    Line& operator=(const Line& other) = default;
    Line& operator=(Line&& other) = default;

    bool IsParallelTo(const Line& other) const {
        return vector_.IsCollinearTo(other.vector_);
    }

    bool operator==(const Line& other) const {
        return (IsParallelTo(other) && vector_.IsCollinearTo(point_ - other.point_));
    }

    bool operator!=(const Line& other) const {
        return !(*this == other);
    }

    bool IsIntersectWith(const Line& other) const {
        return (*this == other || !IsParallelTo(other));
    }

    // numerator and denominator of t: x = point_ + t * vector_, where x - point of intersection
    std::pair<T, T> Intersection(const Line& other) const {
        T numerator = other.vector_ % (point_ - other.point_);
        T denominator = vector_ % other.vector_;
        if (denominator < 0) {
            denominator *= -1;
            numerator *= -1;
        }
        return std::make_pair(numerator, denominator);
    }

    // numerator and denominator of coordinate t: point = point_ + t * vector_
    std::pair<T, T> CoordinateOfPoint(const Point<T>& point) const {
        T numerator = point.GetX() - point_.GetX();
        T denominator = vector_.GetX();
        if (denominator < 0) {
            denominator *= -1;
            numerator *= -1;
        }
        return std::make_pair(numerator, denominator);
    }
private:
    // L = {a + t*v: t \in R}, a - point, v - vector, v != 0
    Point<T> point_;
    Vector<T> vector_;
};


template <typename T>
class Segment {
public:
    Segment() = default;
    Segment(const Point<T>& point, const Vector<T>& vector): point_(point), vector_(vector) {}
    Segment(const Segment& other) = default;
    Segment(Segment&& other) = default;

    Segment& operator=(const Segment& other) = default;
    Segment& operator=(Segment&& other) = default;

    bool IsIntersectWith(const Line<T>& other_line) const {
        Line<T> line(point_, vector_);
        if (line.IsParallelTo(other_line)) {
            return (line == other_line);
        }
        auto pair = line.Intersection(other_line);
        T numerator = pair.first;
        T denominator = pair.second;
        return (numerator >= 0 && numerator <= denominator);
    }

    bool IsIntersectWith(const Segment& other) const {
        Line<T> line(point_, vector_);
        Line<T> other_line(other.point_, other.vector_);
        if (line.IsParallelTo(other_line)) {
            if (line != other_line) {
                return false;
            }
            auto pair = line.CoordinateOfPoint(other.point_);
            T numerator_start = pair.first;
            T denominator_start = pair.second;
            pair = line.CoordinateOfPoint(other.point_ + other.vector_);
            T numerator_end = pair.first;
            T denominator_end = pair.second;
            // we want t_start <= t_end
            if (numerator_start * denominator_end > numerator_end * denominator_start) {
                std::swap(numerator_start, numerator_end);
                std::swap(denominator_start, denominator_end);
            }
            // [0, 1] \cap [t_start, t_end] != \varnothing <=> (t_start <= 1 and t_end >= 0)
            return (numerator_start <= denominator_start && numerator_end >= 0);
        }
        return (IsIntersectWith(other_line) && other.IsIntersectWith(line));
    }

private:
    // S = {a + t*v: t \in [0, 1]}, a - point, v - vector
    Point<T> point_;
    Vector<T> vector_;
};


template <typename T>
class Solution {
public:
    Solution(std::vector<Point<T>>&& polygon_1, std::vector<Point<T>>&& polygon_2): polygon_1_(std::move(polygon_1)), polygon_2_(std::move(polygon_2)) {}

    bool IsIntersect() {
        if (polygon_1_.empty() || polygon_2_.empty()) {
            return true;
        }
        for (auto& point: polygon_2_) {
            point *= -1;
        }
        FindSum();
        T N = 1000000009;
        Vector<T> a(N, N + 1);
        Segment<T> line(Point<T>(0, 0), a);
        int count = 0;
        for (int i = 0; i < polygon_.size(); ++i) {
            int j = (i + 1) % polygon_.size();
            Segment<T> edge(polygon_[i], polygon_[j] - polygon_[i]);
            if (edge.IsIntersectWith(line)) {
                ++count;
            }
        }
        return (count == 1);
    }
private:
    std::vector<Point<T>> polygon_1_;
    std::vector<Point<T>> polygon_2_;
    std::vector<Point<T>> polygon_;

    void FindSum() {
        int i_0 = 0;
        int j_0 = 0;
        for (int i = 0; i < polygon_1_.size(); ++i) {
            if (polygon_1_[i_0].GetY() > polygon_1_[i].GetY() ||
                (polygon_1_[i_0].GetY() == polygon_1_[i].GetY() && polygon_1_[i_0].GetX() > polygon_1_[i].GetX())) {
                i_0 = i;
            }
        }
        for (int j = 0; j < polygon_2_.size(); ++j) {
            if (polygon_2_[j_0].GetY() > polygon_2_[j].GetY() ||
                (polygon_2_[j_0].GetY() == polygon_2_[j].GetY() && polygon_2_[j_0].GetX() > polygon_2_[j].GetX())) {
                j_0 = j;
            }
        }
        int i = i_0;
        int j = j_0;
        int count_1 = 0;
        int count_2 = 0;
        polygon_.push_back(polygon_1_[i_0] + polygon_2_[j_0]);
        while (true) {
            if (count_1 < polygon_1_.size() && count_2 < polygon_2_.size()) {
                int next_i = (i + 1) % polygon_1_.size();
                int next_j = (j + 1) % polygon_2_.size();
                auto vector_1 = polygon_1_[next_i] - polygon_1_[i];
                auto vector_2 = polygon_2_[next_j] - polygon_2_[j];
                if (vector_1 % vector_2 < 0) {
                    polygon_.push_back(polygon_.back() + vector_1);
                    ++count_1;
                    i = next_i;
                } else {
                    polygon_.push_back(polygon_.back() + vector_2);
                    ++count_2;
                    j = next_j;
                }
            } else if (count_1 < polygon_1_.size()) {
                int next_i = (i + 1) % polygon_1_.size();
                auto vector = polygon_1_[next_i] - polygon_1_[i];
                polygon_.push_back(polygon_.back() + vector);
                ++count_1;
                i = next_i;
            } else if (count_2 < polygon_2_.size()) {
                int next_j = (j + 1) % polygon_2_.size();
                auto vector = polygon_2_[next_j] - polygon_2_[j];
                polygon_.push_back(polygon_.back() + vector);
                ++count_2;
                j = next_j;
            } else {
                break;
            }
        }
    }
};


int main() {
    int n;
    std::cin >> n;
    std::vector<Point<long double>> polygon_1(n);
    for (auto& point: polygon_1) {
        std::cin >> point;
    }
    int m;
    std::cin >> m;
    std::vector<Point<long double>> polygon_2(m);
    for (auto& point: polygon_2) {
        std::cin >> point;
    }
    Solution<long double> solution(std::move(polygon_1), std::move(polygon_2));
    std::cout << (solution.IsIntersect() ? "YES" : "NO") << std::endl;
}
