#include <iostream>
#include <vector>
#include <algorithm>
#include <cmath>
#include <unordered_map>
#include <list>


template <typename T>
class Vector {
public:
    explicit Vector(T x = 0, T y = 0, T z = 0): x_(x), y_(y), z_(z){}

    Vector& operator+=(const Vector& other) {
        x_ += other.x_;
        y_ += other.y_;
        z_ += other.z_;
        return *this;
    }

    Vector& operator-=(const Vector& other) {
        x_ -= other.x_;
        y_ -= other.y_;
        z_ -= other.z_;
        return *this;
    }

    Vector& operator*=(T coefficient) {
        x_ *= coefficient;
        y_ *= coefficient;
        z_ *= coefficient;
        return *this;
    }

    Vector& operator/=(T coefficient) {
        x_ /= coefficient;
        y_ /= coefficient;
        z_ /= coefficient;
        return *this;
    }

    T Dot(const Vector& other) const {
        return (x_ * other.x_ + y_ * other.y_ + z_ * other.z_);
    }

    Vector operator%(const Vector& other) const {
        return Vector(y_ * other.z_ - z_ * other.y_, z_ * other.x_ - x_ * other.z_, x_ * other.y_ - y_ * other.x_);
    }

    Vector operator+(const Vector& other) const {
        auto result = *this;
        result += other;
        return result;
    }

    Vector operator-(const Vector& other) const {
        auto result = *this;
        result -= other;
        return result;
    }

    Vector operator*(T coefficient) {
        auto result = *this;
        result *= coefficient;
        return result;
    }

    Vector operator/(T coefficient) {
        auto result = *this;
        result /= coefficient;
        return result;
    }

    bool operator==(const Vector& other) const {
        return (x_ == other.x_ && y_ == other.y_ && z_ == other.z_);
    }

    bool IsCollinearTo(const Vector& other) const {
        return (*this % other == Vector(0, 0, 0));
    }

    T GetX() const {
        return x_;
    }

    T GetY() const {
        return y_;
    }

    T GetZ() const {
        return z_;
    }

private:
    T x_;
    T y_;
    T z_;
};


template <typename T>
T Dot(const Vector<T>& left, const Vector<T>& right) {
    return left.Dot(right);
}


template <typename T>
Vector<T> operator*(T coefficient, const Vector<T>& vector) {
    return vector * coefficient;
}


template <typename T>
bool IsCollinear(const Vector<T>& left, const Vector<T>& right) {
    return left.IsCollinearTo(right);
}


template <typename T>
using Point = Vector<T>;


template <typename T>
std::istream& operator>>(std::istream& in, Point<T>& point) {
    T x;
    T y;
    T z;
    in >> x >> y >> z;
    point = Point<T>(x, y, z);
    return in;
}


template <typename T>
std::ostream& operator<<(std::ostream& out, const Point<T>& point) {
    out << point.GetX() << " " << point.GetY() << " " << point.GetZ();
    return out;
}


template <typename T>
class Segment {
public:
    Segment() = default;
    Segment(const Point<T>& point, const Vector<T>& vector): point_(point), vector_(vector) {}


    bool operator==(const Segment& other) const {
        return (point_ == other.point_ && vector_ == other.vector_) || (point_ == other.point_ + other.vector_ && other.point_ == point_ + vector_);
    }

    bool operator!=(const Segment& other) const {
        return !(*this == other);
    }

    Point<T> GetPoint() const {
        return point_;
    }

    Vector<T> GetVector() const {
        return vector_;
    }

private:
    // S = {a + t*v: t \in [0, 1]}, a - point, v - vector
    Point<T> point_;
    Vector<T> vector_;
};


template <typename T>
class Face {
public:
    explicit Face(const Point<T>& p_1 = Point<T>(), const Point<T>& p_2 = Point<T>(), const Point<T>& p_3= Point<T>()): point_1_(p_1), point_2_(p_2), point_3_(p_3) {}

    Vector<T> Normal() const {
        return (point_2_ - point_1_) % (point_3_ - point_2_);
    }

    bool IsVisibleFrom(const Point<T>& point) const {
        return Dot(point - point_1_, Normal()) > 0;
    }

    void ChangeOrientation() {
        std::swap(point_2_, point_3_);
    }

    Point<T> GetPoint1() const {
        return point_1_;
    }

    Point<T> GetPoint2() const {
        return point_2_;
    }

    Point<T> GetPoint3() const {
        return point_3_;
    }

private:
    Point<T> point_1_;
    Point<T> point_2_;
    Point<T> point_3_;

};


template <typename T>
struct SegmentHash {
    std::size_t operator()(const Segment<T>& segment) const {
        auto point = segment.GetPoint();
        auto vector = segment.GetVector();
        if (vector.GetX() < 0 || (vector.GetX() == 0 && vector.GetY() < 0) || (vector.GetX() == 0 && vector.GetY() == 0 && vector.GetZ() < 0)) {
            point = point + vector;
            vector *= -1;
        }
        std::vector<T> numbers = {point.GetX(), point.GetY(), point.GetZ(), vector.GetX(), vector.GetY(), vector.GetZ()};
        std::size_t p = 2;
        std::size_t q = 100000007;
        std::size_t hash = 0;
        for (auto i: numbers) {
            hash = (hash * p + (i + 500)) % q;
        }
        return hash;
    }
};


template <typename T>
std::ostream& operator<<(std::ostream& out, const Face<T>& face) {
    out << face.GetPoint1() << " " << face.GetPoint2() << " " << face.GetPoint3();
    return out;
}


template <typename T>
class Solution {
public:
    explicit Solution(std::vector<Point<T>>&& points): points_(std::move(points)) {}

    std::vector<std::vector<int>> CalculateConvexHull() {
        ConstructInitialTetrahedron();
        for (int i = 4; i < points_.size(); ++i) {
            point_ = points_[i];
            FindAndDeleteVisibleFaces();
            FindBoundary();
            AddNewFaces();
        }
        ConstructAnswer();
        return answer_;
    }

private:
    std::vector<Point<T>> points_;
    std::list<Face<T>> faces_;
    std::list<Face<T>> visibleFaces_;
    std::unordered_map<Segment<T>, int, SegmentHash<T>> segments_;
    std::unordered_map<Segment<T>, Face<T>, SegmentHash<T>> unique_segments_;
    Point<T> point_;
    std::vector<std::vector<int>> answer_;

    void ConstructInitialTetrahedron() {
        for (int p = 0; p < 4; ++p) {
            int i = 0;
            int j = 1;
            int k = 2;
            if (i == p) {
                ++i;
                ++j;
                ++k;
            } else if (j == p) {
                ++j;
                ++k;
            } else if (k == p) {
                ++k;
            }
            faces_.emplace_back(points_[i], points_[j], points_[k]);
            if (faces_.back().IsVisibleFrom(points_[p])) {
                faces_.back().ChangeOrientation();
            }
        }
    }

    void FindAndDeleteVisibleFaces() {
        visibleFaces_.clear();
        auto iter = faces_.begin();
        while (iter != faces_.end()) {
            if (iter->IsVisibleFrom(point_)) {
                visibleFaces_.push_back(*iter);
                iter = faces_.erase(iter);
            } else {
                ++iter;
            }
        }
    }

    void FindBoundary() {
        segments_.clear();
        for (const auto& face: visibleFaces_) {
            Segment<T> segment_1(face.GetPoint1(), face.GetPoint2() - face.GetPoint1());
            Segment<T> segment_2(face.GetPoint2(), face.GetPoint3() - face.GetPoint2());
            Segment<T> segment_3(face.GetPoint3(), face.GetPoint1() - face.GetPoint3());
            ++segments_[segment_1];
            ++segments_[segment_2];
            ++segments_[segment_3];
        }
        unique_segments_.clear();
        for (const auto& face: visibleFaces_) {
            Segment<T> segment_1(face.GetPoint1(), face.GetPoint2() - face.GetPoint1());
            Segment<T> segment_2(face.GetPoint2(), face.GetPoint3() - face.GetPoint2());
            Segment<T> segment_3(face.GetPoint3(), face.GetPoint1() - face.GetPoint3());
            if (segments_[segment_1] == 1) {
                unique_segments_[segment_1] = face;
            }
            if (segments_[segment_2] == 1) {
                unique_segments_[segment_2] = face;
            }
            if (segments_[segment_3] == 1) {
                unique_segments_[segment_3] = face;
            }
        }
    }

    void AddNewFaces() {
        for (const auto& p: unique_segments_) {
            auto point_1 = p.first.GetPoint();
            auto point_2 = point_1 + p.first.GetVector();
            Face<T> new_face(point_1, point_2, point_);
            Point<T> other_point;
            auto old_face = p.second;
            if (old_face.GetPoint1() == point_1 || old_face.GetPoint1() == point_2) {
                if (old_face.GetPoint2() == point_1 || old_face.GetPoint2() == point_2) {
                    other_point = old_face.GetPoint3();
                } else {
                    other_point = old_face.GetPoint2();
                }
            } else {
                other_point = old_face.GetPoint1();
            }
            if (new_face.IsVisibleFrom(other_point)) {
                new_face.ChangeOrientation();
            }
            faces_.push_back(new_face);
        }
    }

    void ConstructAnswer() {
        answer_.resize(faces_.size());
        std::vector<Face<T>> faces_vector(faces_.begin(), faces_.end());
        for (int i = 0; i < answer_.size(); ++i) {
            for (int j = 0; j < points_.size(); ++j) {
                if (points_[j] == faces_vector[i].GetPoint1() || points_[j] == faces_vector[i].GetPoint2() ||
                    points_[j] == faces_vector[i].GetPoint3()) {
                    answer_[i].push_back(j);
                }
            }
            int k = 0;
            while (std::find(answer_[i].begin(), answer_[i].end(), k) != answer_[i].end()) {
                ++k;
            }
            Face<T> face(points_[answer_[i][0]], points_[answer_[i][1]], points_[answer_[i][2]]);
            if (face.IsVisibleFrom(points_[k])) {
                std::reverse(answer_[i].begin(), answer_[i].end());
            }
            if (answer_[i][1] < answer_[i][0] && answer_[i][1] < answer_[i][2]) {
                answer_[i] = {answer_[i][1], answer_[i][2], answer_[i][0]};
            }
            if (answer_[i][2] < answer_[i][0] && answer_[i][2] < answer_[i][1]) {
                answer_[i] = {answer_[i][2], answer_[i][0], answer_[i][1]};
            }
        }
        std::sort(answer_.begin(), answer_.end());
    }
};



int main() {
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(nullptr);
    std::cout.tie(nullptr);
    int m;
    std::cin >> m;
    for (int i = 0; i < m; ++i) {
        int n;
        std::cin >> n;
        std::vector<Point<int>> points(n);
        for (int j = 0; j < n; ++j) {
            std::cin >> points[j];
        }
        Solution<int> solution(std::move(points));
        auto faces = solution.CalculateConvexHull();
        std::cout << faces.size() << std::endl;
        for (const auto& face: faces) {
            std::cout << face.size();
            for (auto p: face) {
                std::cout << " " << p;
            }
            std::cout << std::endl;
        }
    }
}
