#include <iostream>
#include <vector>
#include <array>


const int FIELD_SIZE = 8;


class Game {
public:
    explicit Game(const std::array<std::array<int, FIELD_SIZE>, FIELD_SIZE>& initial_matrix): matrix_(initial_matrix) {
        for (int i = 0; i < FIELD_SIZE; ++i) {
            for (int j = 0; j < FIELD_SIZE; ++j) {
                if (matrix_[i][j] == 2) {
                    escapee_start = i * FIELD_SIZE + j;
                    matrix_[i][j] = 0;
                }
                if (matrix_[i][j] == 3) {
                    terminator_start = i * FIELD_SIZE + j;
                    matrix_[i][j] = 0;
                }
            }
        }
        for (int i = 0; i < FIELD_SIZE * FIELD_SIZE; ++i) {
            for (int j = 0; j < FIELD_SIZE * FIELD_SIZE; ++j) {
                for (int k = 0; k < 2; ++k) {
                    win[i][j][k] = false;
                    loose[i][j][k] = false;
                    used[i][j][k] = false;
                    degree[i][j][k] = 0;
                }
            }
        }
        for (int terminator = 0; terminator < FIELD_SIZE * FIELD_SIZE; ++terminator) {
            for (int escapee = 0; escapee < FIELD_SIZE * FIELD_SIZE; ++escapee) {
                for (int is_terminator_step = 0; is_terminator_step <= 1; ++is_terminator_step) {
                    int terminator_x = terminator / FIELD_SIZE;
                    int terminator_y = terminator % FIELD_SIZE;
                    int escapee_x = escapee / FIELD_SIZE;
                    int escapee_y = escapee % FIELD_SIZE;
                    if (matrix_[terminator_x][terminator_y] == 1 || matrix_[escapee_x][escapee_y] == 1) {
                        continue;
                    }
                    bool& current_win = win[terminator][escapee][is_terminator_step];
                    bool& current_loose = loose[terminator][escapee][is_terminator_step];
                    if (is_terminator_step) {
                        current_win = TerminatorWin(terminator_x, terminator_y, escapee_x, escapee_y);
                    } else {
                        current_win = (escapee_x == FIELD_SIZE - 1 && !TerminatorWin(terminator_x, terminator_y, escapee_x, escapee_y));
                        current_loose = TerminatorWin(terminator_x, terminator_y, escapee_x, escapee_y);
                    }
                    if (!current_win && !current_loose) {
                        ConstructEdges(terminator, escapee, is_terminator_step);
                    }

                }
            }
        }
        for (int terminator = 0; terminator < FIELD_SIZE * FIELD_SIZE; ++terminator) {
            for (int escapee = 0; escapee < FIELD_SIZE * FIELD_SIZE; ++escapee) {
                for (char is_terminator_step = 0; is_terminator_step <= 1; ++is_terminator_step) {
                    if ((win[terminator][escapee][is_terminator_step] || loose[terminator][escapee][is_terminator_step]) && !used[terminator][escapee][is_terminator_step]) {
                        DFS(terminator, escapee, is_terminator_step);
                    }
                }
            }
        }
    }

    bool IsWinning() {
        return win[terminator_start][escapee_start][false];
    }

private:
    std::array<std::array<int, FIELD_SIZE>, FIELD_SIZE> matrix_;
    struct State {
        int terminator;
        int escapee;
        int is_terminator_step;
    };

    int terminator_start;
    int escapee_start;

    std::vector<State> previous_states[100][100][2];
    std::array<std::array<std::array<bool, 2>, FIELD_SIZE * FIELD_SIZE>, FIELD_SIZE * FIELD_SIZE> win;
    std::array<std::array<std::array<bool, 2>, FIELD_SIZE * FIELD_SIZE>, FIELD_SIZE * FIELD_SIZE> loose;
    std::array<std::array<std::array<bool, 2>, FIELD_SIZE * FIELD_SIZE>, FIELD_SIZE * FIELD_SIZE> used;
    std::array<std::array<std::array<int, 2>, FIELD_SIZE * FIELD_SIZE>, FIELD_SIZE * FIELD_SIZE> degree;


    void DFS(int terminator, int escapee, int is_terminator_step) {
        used[terminator][escapee][is_terminator_step] = true;
        for (const auto& previous_state: previous_states[terminator][escapee][is_terminator_step]) {
            if (!used[previous_state.terminator][previous_state.escapee][previous_state.is_terminator_step]) {
                if (loose[terminator][escapee][is_terminator_step]) {
                    win[previous_state.terminator][previous_state.escapee][previous_state.is_terminator_step] = true;
                }
                else if (--degree[previous_state.terminator][previous_state.escapee][previous_state.is_terminator_step] == 0) {
                    loose[previous_state.terminator][previous_state.escapee][previous_state.is_terminator_step] = true;
                }
                else {
                    continue;
                }
                DFS(previous_state.terminator, previous_state.escapee, previous_state.is_terminator_step);
            }
        }
    }

    bool TerminatorWin(int terminator_x, int terminator_y, int escapee_x, int escapee_y) {
        int i = 0;
        int x_coefficient = 0;
        int y_coefficient = 0;
        if (terminator_x < escapee_x) {
            x_coefficient = 1;
        } else if (terminator_x > escapee_x) {
            x_coefficient = -1;
        }
        if (terminator_y < escapee_y) {
            y_coefficient = 1;
        } else if (terminator_y > escapee_y) {
            y_coefficient = -1;
        }
        while (terminator_x + i * x_coefficient < FIELD_SIZE && terminator_x + i * x_coefficient >= 0 &&
               terminator_y + i * y_coefficient < FIELD_SIZE && terminator_y + i * y_coefficient >= 0) {
            if (terminator_x + i * x_coefficient == escapee_x && terminator_y + i * y_coefficient == escapee_y) {
                return true;
            }
            if (matrix_[terminator_x + i * x_coefficient][terminator_y + i * y_coefficient] == 1) {
                return false;
            }
            ++i;
        }
        return false;
    }

    void ConstructEdges(int terminator, int escapee, int is_terminator_step) {
        int terminator_x = terminator / FIELD_SIZE;
        int terminator_y = terminator % FIELD_SIZE;
        int escapee_x = escapee / FIELD_SIZE;
        int escapee_y = escapee % FIELD_SIZE;
        State state = {terminator, escapee, is_terminator_step};
        std::array<int, 8> delta_x = {-1, 0, 1, 0, -1, -1, 1, 1};
        std::array<int, 8> delta_y = {0, 1, 0, -1, -1, 1, -1, 1};
        for (int i = 0; i < 8; ++i) {
            int new_terminator_x = terminator_x;
            int new_terminator_y = terminator_y;
            int new_escapee_x = escapee_x;
            int new_escapee_y = escapee_y;
            if (is_terminator_step) {
                new_terminator_x += delta_x[i];
                new_terminator_y += delta_y[i];
            }
            else {
                new_escapee_x += delta_x[i];
                new_escapee_y += delta_y[i];
            }

            if (new_terminator_x >= 0 && new_terminator_x < FIELD_SIZE && new_terminator_y >= 0 && new_terminator_y < FIELD_SIZE && matrix_[new_terminator_x][new_terminator_y] != 1 &&
                new_escapee_x >= 0 && new_escapee_x < FIELD_SIZE && new_escapee_y >= 0 && new_escapee_y < FIELD_SIZE && matrix_[new_escapee_x][new_escapee_y] != 1) {
                previous_states[new_terminator_x * FIELD_SIZE + new_terminator_y][new_escapee_x * FIELD_SIZE + new_escapee_y][!is_terminator_step].push_back(state);
                ++degree[terminator][escapee][is_terminator_step];
            }
        }
    }
};



int main() {
    std::array<std::array<int, FIELD_SIZE>, FIELD_SIZE> matrix;
    for (int i = 0; i < FIELD_SIZE; ++i) {
        for (int j = 0; j < FIELD_SIZE; ++j) {
            char symbol;
            std::cin >> symbol;
            matrix[i][j] = symbol - '0';
        }
    }
    Game game(matrix);
    std::cout << (game.IsWinning() ? 1 : -1);
}
