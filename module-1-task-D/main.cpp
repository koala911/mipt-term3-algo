#include <iostream>
#include <cstdint>
#include <vector>
#include <deque>


class Solution {
public:
    explicit Solution(const std::string& text);

    int64_t FindSubstrings();

private:
    struct Symbol {
        unsigned char symbol;
        bool is_terminal;

        Symbol(): symbol(0), is_terminal(true) {}
        explicit Symbol(char c): symbol(c), is_terminal(false) {}
        explicit Symbol(bool terminal): symbol(0), is_terminal(terminal) {}

        bool operator==(const Symbol& other) const;
        bool operator!=(const Symbol& other) const;

        int Number() const;
    };

    std::vector<Symbol> text_;
    std::vector<int> LCP_;
    std::deque<int> suffix_array_;
    std::vector<int> count_;
    std::vector<int> classes_;
    int number_of_classes_;
    const int alphabet_size_ = 257; // 256 in char + terminal


    void CalculateSuffixMassive();
    void CalculateBase();
    void CalculateLCP();
    void AddTerminalSymbols();
    void DeleteTerminalSymbols();
};


Solution::Solution(const std::string& text) {
    for (char symbol: text) {
        text_.emplace_back(Symbol(symbol));
    }
}


int64_t Solution::FindSubstrings() {
    AddTerminalSymbols();
    CalculateSuffixMassive();
    DeleteTerminalSymbols();
    CalculateLCP();
    int64_t answer = 0;
    for (int i = 0; i < text_.size(); ++i) {
        answer += text_.size() - suffix_array_[i] - LCP_[i];
    }
    return answer - 1;
}


bool Solution::Symbol::operator==(const Symbol& other) const {
    return (is_terminal == other.is_terminal) && (symbol == other.symbol);
}

bool Solution::Symbol::operator!=(const Symbol& other) const {
    return (symbol != other.symbol) || (is_terminal != other.is_terminal);
}


int Solution::Symbol::Number() const {
    if (is_terminal) {
        return 0;
    } else {
        return symbol + 1;
    }
}


void Solution::CalculateSuffixMassive() {
    CalculateBase();
    std::vector<int> permutation_second(text_.size());
    std::vector<int> new_classes(text_.size());
    // m = 2^k at k-th iteration
    for (int m = 1; m < text_.size(); m *= 2) {
        for (int i = 0; i < text_.size(); ++i) {
            permutation_second[i] = suffix_array_[i] - m;
            if (permutation_second[i] < 0) {
                permutation_second[i] += text_.size();
            }
        }
        count_.assign(number_of_classes_, 0);
        for (int i = 0; i < text_.size(); ++i) {
            ++count_[classes_[permutation_second[i]]];
        }
        for (int i = 1; i < number_of_classes_; ++i) {
            count_[i] += count_[i - 1];
        }
        for (int i = text_.size() - 1; i >= 0; --i) {
            --count_[classes_[permutation_second[i]]];
            suffix_array_[count_[classes_[permutation_second[i]]]] = permutation_second[i];
        }
        new_classes[suffix_array_[0]] = 0;
        number_of_classes_ = 1;
        for (int i = 1; i < text_.size(); ++i) {
            int middle_i = (suffix_array_[i] + m) % text_.size();
            int middle_prev = (suffix_array_[i - 1] + m) % text_.size();
            if (classes_[suffix_array_[i]] != classes_[suffix_array_[i - 1]] ||
                    classes_[middle_i] != classes_[middle_prev]) {
                ++number_of_classes_;
            }
            new_classes[suffix_array_[i]] = number_of_classes_ - 1;
        }
        classes_ = new_classes;
    }
}


void Solution::CalculateBase() {
    suffix_array_.resize(text_.size());
    count_.resize(std::max(int(text_.size()), alphabet_size_), 0);
    classes_.resize(text_.size());
    for (const auto& symbol: text_) {
        ++count_[symbol.Number()];
    }
    for (int i = 1; i < alphabet_size_; ++i) {
        count_[i] += count_[i - 1];
    }
    for (int i = 0; i < text_.size(); ++i) {
        --count_[text_[i].Number()];
        suffix_array_[count_[text_[i].Number()]] = i;
    }
    classes_[suffix_array_[0]] = 0;
    number_of_classes_ = 1;
    for (int i = 1; i < text_.size(); ++i) {
        if (text_[suffix_array_[i]] != text_[suffix_array_[i - 1]]) {
            ++number_of_classes_;
        }
        classes_[suffix_array_[i]] = number_of_classes_ - 1;
    }
}


void Solution::CalculateLCP() {
    LCP_.resize(text_.size());
    std::vector<int> reverse_suffix_array(text_.size());
    for (int i = 0; i < text_.size(); ++i) {
        reverse_suffix_array[suffix_array_[i]] = i;
    }
    int current = 0;
    for (int i = 0; i < text_.size(); ++i) {
        if (current > 0) {
            --current;
        }
        if (reverse_suffix_array[i] == text_.size() - 1) {
            LCP_[text_.size() - 1] = -1;
            current = 0;
        } else {
            int j = suffix_array_[reverse_suffix_array[i] + 1];
            while (std::max(i + current, j + current) < text_.size() &&
                   text_[i + current] == text_[j + current]) {
                ++current;
            }
            LCP_[reverse_suffix_array[i]] = current;
        }
    }
}


void Solution::AddTerminalSymbols() {
    text_.emplace_back(true);
    int new_size = 1;
    while (new_size < text_.size()) {
        new_size *= 2;
    }
    while (text_.size() < new_size) {
        text_.emplace_back(true);
    }
}


void Solution::DeleteTerminalSymbols() {
    int new_size = 0;
    while (!text_[new_size].is_terminal) {
        ++new_size;
    }
    for (int i = 0; i < text_.size() - new_size; ++i) {
        suffix_array_.pop_front();
    }
    text_.resize(new_size);
}


int main() {
    std::string text;
    std::cin >> text;
    Solution solution(text);
    std::cout << solution.FindSubstrings();
}
