#include <cmath>
#include <cstdio>
#include <iostream>
#include <complex>
#include <vector>
#include <cstdio>


const double PI = 3.1415926535;

// Структура, описывающая заголовок WAV файла.
struct WAVHeader {
    // WAV-формат начинается с RIFF-заголовка:

    // Содержит символы "RIFF" в ASCII кодировке
    // (0x52494646 в big-endian представлении)
    char chunk_id[4];

    // 36 + subchunk2_size, или более точно:
    // 4 + (8 + subchunk1_size) + (8 + subchunk2_size)
    // Это оставшийся размер цепочки, начиная с этой позиции.
    // Иначе говоря, это размер файла - 8, то есть,
    // исключены поля chunk_id и chunk_size.
    unsigned int chunk_size;

    // Содержит символы "WAVE"
    // (0x57415645 в big-endian представлении)
    char format[4];

    // Формат "WAVE" состоит из двух подцепочек: "fmt " и "data":
    // Подцепочка "fmt " описывает формат звуковых данных:

    // Содержит символы "fmt "
    // (0x666d7420 в big-endian представлении)
    char subchunk1_id[4];

    // 16 для формата PCM.
    // Это оставшийся размер подцепочки, начиная с этой позиции.
    unsigned int subchunk1_size;

    // Аудио формат, полный список можно получить здесь http://audiocoding.ru/wav_formats.txt
    // Для PCM = 1 (то есть, Линейное квантование).
    // Значения, отличающиеся от 1, обозначают некоторый формат сжатия.
    unsigned short audio_format;

    // Количество каналов. Моно = 1, Стерео = 2 и т.д.
    unsigned short number_of_channels;

    // Частота дискретизации. 8000 Гц, 44100 Гц и т.д.
    unsigned int sample_rate;

    // sample_rate * number_of_channels * bits_per_sample / 8
    unsigned int byte_rate;

    // number_of_channels * bits_per_sample / 8
    // Количество байт для одного сэмпла, включая все каналы.
    unsigned short block_align;

    // Так называемая "глубиная" или точность звучания. 8 бит, 16 бит и т.д.
    unsigned short bits_per_sample;

    // Подцепочка "data" содержит аудио-данные и их размер.

    // Содержит символы "data"
    // (0x64617461 в big-endian представлении)
    char subchunk2_id[4];

    // number_of_samples * number_of_channels * bits_per_sample / 8
    // Количество байт в области данных.
    unsigned int subchunk2_size;

    // Далее следуют непосредственно Wav данные.
};

void FFT(std::vector<std::complex<double>>& data, bool inverse) {
    int size = data.size();
    if (size == 1) {
        return;
    }
    std::vector<std::complex<double>> data_even(size / 2);
    std::vector<std::complex<double>> data_odd(size / 2);
    for (int i = 0, j = 0; i < size; i += 2, ++j) {
        data_even[j] = data[i];
        data_odd[j] = data[i + 1];
    }
    FFT(data_even, inverse);
    FFT(data_odd, inverse);
    double angle = 2 * PI / size * (inverse ? -1 : 1);
    std::complex<double> root(1); // e^(2pi * k * i / n)
    std::complex<double> primitive_root(cos(angle), sin(angle)); // e^(2pi * i / n)
    for (int i = 0; i < size / 2; ++i) {
        data[i] = data_even[i] + root * data_odd[i];
        data[i + size / 2] = data_even[i] - root * data_odd[i];
        if (inverse) {
            data[i] /= 2;
            data[i + size / 2] /= 2;
        }
        root *= primitive_root;
    }
}

void ExpandData(std::vector<std::complex<double>>& data_vector) {
    int new_size = 1;
    while (new_size < data_vector.size()) {
        new_size *= 2;
    }
    while (data_vector.size() < new_size) {
        data_vector.emplace_back(0);
    }
}

void ReadHeader(WAVHeader& header, FILE* file) {
    fread(&header, sizeof(WAVHeader), 1, file);

    // Выводим полученные данные
    std::cout << header.chunk_id[0] << header.chunk_id[1] << header.chunk_id[2] << header.chunk_id[3] << std::endl;
    std::cout << "Chunk size: " << header.chunk_size << std::endl;
    std::cout << header.format[0] << header.format[1] << header.format[2] << header.format[3] << std::endl;
    std::cout << header.subchunk1_id[0] << header.subchunk1_id[1] << header.subchunk1_id[2] << header.subchunk1_id[3] << std::endl;
    std::cout << "SubChunkId1: " << header.subchunk1_size << std::endl;
    std::cout << "Audio format: " << header.audio_format << std::endl;
    std::cout << "Channels: " << header.number_of_channels << std::endl;
    std::cout << "Sample rate: " << header.sample_rate << std::endl;
    std::cout << "Bits per sample: " << header.bits_per_sample << std::endl;
    std::cout << header.subchunk2_id[0] << header.subchunk2_id[1] << header.subchunk2_id[2] << header.subchunk2_id[3] << std::endl;

    // Посчитаем длительность воспроизведения в секундах
    float duration_seconds = 1.f * header.subchunk2_size / (header.bits_per_sample / 8) / header.number_of_channels / header.sample_rate;
    int duration_minutes = int(floor(duration_seconds)) / 60;
    duration_seconds = duration_seconds - (duration_minutes * 60);
    printf("Duration: %02d:%02.f\n", duration_minutes, duration_seconds);
}

void CompressData(char* data, int data_size) {
    std::vector<std::complex<double>> data_vector(data_size);
    for (int i = 0; i < data_size; ++i) {
        data_vector[i] = data[i];
    }
    ExpandData(data_vector);
    FFT(data_vector, false);
    double zeroing_part = 0.3;
    for (int i = 0; i < data_size; ++i) {
        if (i > (1 - zeroing_part) * data_size) {
            data_vector[i] = 0;
        }
    }
    FFT(data_vector, true);
    for (int i = 0; i < data_size; ++i) {
        data[i] = char(data_vector[i].real());
    }
}

void WAVCompressor(char* file_name, char* compressed_file_name) {
    FILE *file = fopen(file_name, "r");
    if (!file) {
        std::cout << "Failed open file";
        return;
    }

    WAVHeader header;
    ReadHeader(header, file);

    char* data = new char[header.subchunk2_size];
    fread(data, header.subchunk2_size, 1, file);
    std::cout << "Data is successfully loaded." << std::endl;

    FILE *compressed_file = fopen(compressed_file_name, "w");
    fwrite(&header, sizeof(header), 1, compressed_file);

    int data_size = header.subchunk2_size;
    CompressData(data, data_size);
    fwrite(data, data_size, 1, compressed_file);

    delete[] data;
    fclose(file);
    fclose(compressed_file);
}

int main() {
    WAVCompressor("../speech.wav", "../compressed.wav");
}
