#include <iostream>
#include <vector>
#include <algorithm>
#include <cmath>
#include <cstdint>
#include <set>
#include <optional>


template <typename T>
class Vector {
public:
    explicit Vector(T x = 0, T y = 0): x_(x), y_(y) {}


    Vector& operator+=(const Vector& other) {
        x_ += other.x_;
        y_ += other.y_;
        return *this;
    }

    Vector& operator-=(const Vector& other) {
        x_ -= other.x_;
        y_ -= other.y_;
        return *this;
    }

    Vector& operator*=(T coefficient) {
        x_ *= coefficient;
        y_ *= coefficient;
        return *this;
    }

    Vector& operator/=(T coefficient) {
        x_ /= coefficient;
        y_ /= coefficient;
        return *this;
    }

    T Dot(const Vector& other) const {
        return (x_ * other.x_ + y_ * other.y_);
    }

    T operator%(const Vector& other) const {
        return (x_ * other.y_ - y_ * other.x_);
    }

    Vector operator+(const Vector& other) const {
        auto result = *this;
        result += other;
        return result;
    }

    Vector operator-(const Vector& other) const {
        auto result = *this;
        result -= other;
        return result;
    }

    Vector operator*(T coefficient) const {
        auto result = *this;
        result *= coefficient;
        return result;
    }

    Vector operator/(T coefficient) {
        auto result = *this;
        result /= coefficient;
        return result;
    }

    bool operator==(const Vector& other) const {
        return (x_ == other.x_ && y_ == other.y_);
    }

    bool IsCollinearTo(const Vector& other) const {
        return (*this % other == 0);
    }

    T GetX() const {
        return x_;
    }

    T GetY() const {
        return y_;
    }

    T SquareOfLength() const {
        return x_ * x_ + y_ * y_;
    }
private:
    T x_;
    T y_;
};


template <typename T>
T Dot(const Vector<T>& left, const Vector<T>& right) {
    return left.Dot(right);
}


template <typename T>
Vector<T> operator*(T coefficient, const Vector<T>& vector) {
    return vector * coefficient;
}


template <typename T>
bool IsCollinear(const Vector<T>& left, const Vector<T>& right) {
    return left.IsCollinearTo(right);
}


template <typename T>
using Point = Vector<T>;


template <typename T>
std::istream& operator>>(std::istream& in, Point<T>& point) {
    T x;
    T y;
    in >> x >> y;
    point = Point<T>(x, y);
    return in;
}



template <typename T>
class Segment {
public:
    Segment(const Point<T>& point, const Vector<T>& vector, int64_t i): point_(point), vector_(vector), number_(i) {}

    bool operator==(const Segment& other) const {
        return (point_ == other.point_ && vector_ == other.vector_) || (point_ == other.point_ + other.vector_ && other.point_ == point_ + vector_);
    }

    bool operator!=(const Segment& other) const {
        return !(*this == other);
    }

    bool IsIntersectingWith(const Segment& other) const {
        Point<T> point_1 = point_;
        Point<T> point_2 = point_ + vector_;
        Point<T> other_point_1 = other.point_;
        Point<T> other_point_2 = other.point_ + other.vector_;
        return Intersect1D(point_1.GetX(), point_2.GetX(), other_point_1.GetX(), other_point_1.GetX())
               && Intersect1D(point_1.GetY(), point_2.GetY(), other_point_1.GetY(), other_point_2.GetY())
               && Sign(point_1, point_2, other_point_1) * Sign(point_1, point_2, other_point_2) <= 0
               && Sign(other_point_1, other_point_2, point_1) * Sign(other_point_1, other_point_2, point_2) <= 0;
    }

    bool operator<(const Segment& other) const {
        T x = std::max(std::min(point_.GetX(), (point_ + vector_).GetX()), std::min(other.GetPoint().GetX(), (other.GetPoint() + other.GetVector()).GetX()));
        auto p = GetY(x);
        T numerator = p.first;
        T denominator = p.second;
        p = other.GetY(x);
        T other_numerator = p.first;
        T other_denominator = p.second;
        return numerator * other_denominator < other_numerator * denominator;
    }

    Vector<T> GetVector() const {
        return vector_;
    }

    Vector<T> GetPoint() const {
        return point_;
    }

    int64_t GetNumber() const {
        return number_;
    }

private:
    // S = {a + t*v: t \in [0, 1]}, a - point, v - vector
    Point<T> point_;
    Vector<T> vector_;

    int64_t number_;

    std::pair<T, T> GetY(T x) const {
        if (vector_.GetX() == 0) {
            return std::make_pair(point_.GetY(), 1);
        } else {
            T numerator = point_.GetY() * vector_.GetX() + (vector_.GetY() * (x - point_.GetX()));
            T denominator = vector_.GetX();
            if (denominator < 0) {
                numerator *= -1;
                denominator *= -1;
            }
            return std::make_pair(numerator, denominator);
        }
    }

    bool Intersect1D(T left_1, T right_1, T left_2, T right_2) const {
        if (left_1 > right_1) {
            std::swap(left_1, right_1);
        }
        if (left_2 > right_2) {
            std::swap(left_2, right_2);
        }
        return std::max(left_1, left_2) <= std::min(right_1, right_2);
    }

    int Sign(const Point<T>& center, const Point<T>& point_1, const Point<T>& point_2) const {
        T area = (point_1 - center) % (point_2 - center);
        if (area == 0) {
            return 0;
        }
        return area > 0 ? 1 : -1;
    }
};


template <typename T>
class Solution {
public:
    explicit Solution(std::vector<Segment<T>>&& segments): segments_(std::move(segments)) {}

    std::optional<std::pair<int64_t, int64_t>> FindIntersection() {
        std::vector<Event> events;
        for (int64_t i = 0; i < segments_.size(); ++i) {
            events.push_back(Event(std::min(segments_[i].GetPoint().GetX(), (segments_[i].GetPoint() + segments_[i].GetVector()).GetX()), i, true));
            events.push_back(Event(std::max(segments_[i].GetPoint().GetX(), (segments_[i].GetPoint() + segments_[i].GetVector()).GetX()), i, false));
        }
        std::sort(events.begin(), events.end());
        std::vector<typename std::set<Segment<T>>::iterator> segment_position;
        segment_position.resize(segments_.size());
        for (const auto& event: events) {
            int64_t number = event.number;
            if (event.is_start) {
                auto next = segments_set_.lower_bound(segments_[number]);
                auto previous = Previous(next);
                if (next != segments_set_.end() && next->IsIntersectingWith(segments_[number])) {
                    return std::make_pair(next->GetNumber(), number);
                }
                if (previous != segments_set_.end() && previous->IsIntersectingWith(segments_[number])) {
                    return std::make_pair (previous->GetNumber(), number);
                }
                segment_position[number] = segments_set_.insert(next, segments_[number]);
            }
            else {
                auto next = Next(segment_position[number]);
                auto previous = Previous(segment_position[number]);
                if (next != segments_set_.end() && previous != segments_set_.end() && next->IsIntersectingWith(*previous)) {
                    return std::make_pair(previous->GetNumber(), next->GetNumber());
                }
                segments_set_.erase(segment_position[number]);
            }
        }
        return std::nullopt;
    }
private:
    struct Event {
        Event(T x, int64_t i, bool start): x(x), number(i), is_start(start) {}
        T x;
        int64_t number;
        bool is_start;

        bool operator<(const Event& other) const {
            return (x < other.x || (x == other.x && is_start && !other.is_start));
        }
    };

    auto Previous(typename std::set<Segment<T>>::iterator iterator) {
        return iterator == segments_set_.begin() ? segments_set_.end() : --iterator;
    }

    auto Next(typename std::set<Segment<T>>::iterator iterator) {
        return iterator == segments_set_.end() ? segments_set_.end() : ++iterator;
    }

    std::vector<Segment<T>> segments_;
    std::set<Segment<T>> segments_set_;
};


int main() {
    int64_t n;
    std::cin >> n;
    Point<int64_t> start;
    Point<int64_t> end;
    std::vector<Segment<int64_t>> segments;
    for (int64_t i = 0; i < n; ++i) {
        std::cin >> start >> end;
        segments.emplace_back(start, end - start, i);
    }
    Solution<int64_t> solution(std::move(segments));
    auto result = solution.FindIntersection();
    if (result.has_value()) {
        std::cout << "YES" << std::endl;
        auto [i, j] = std::minmax(result.value().first, result.value().second);
        std::cout << i + 1 << " " << j + 1 << std::endl;
    } else {
        std::cout << "NO" << std::endl;
    }
}
